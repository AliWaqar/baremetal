/************************************************************************
 *
 * This is a simple hello world program which prints a test string to the
 * UART of VERSTILE_PB platform
 *
 ***********************************************************************/

#define		UART0_BASE_ADDR		0x101f1000
#define		NULL_TERMINATOR		'\0'

/************************************************************************
 * Global Variables
 ***********************************************************************/

/* This constant pointer is used to access memory mapped uart-0 device */
volatile unsigned int* const UART0DR = (unsigned int *)UART0_BASE_ADDR;

/************************************************************************
 * Function Declartions 
 ***********************************************************************/

/* This function is used to print to the uart-0 device */
void print_uart0(const char* s);

/************************************************************************
 * Function Defintions
 ***********************************************************************/

/* This is the entry point in this c-appliction */
void c_entry(void)
{
	/* Call the print function */
	print_uart0("Hello World!\n");
	
	/* Exit the application */
	return;
}

/* Function for printing to the uart */
void print_uart0(const char* s)
{
	/* Loop until end of string is reached */
	while (*s != NULL_TERMINATOR)
	{
		/* Transmit character */
		*UART0DR = (unsigned int)(*s);

		/* Move to next character in the string */
		s++;
	}

	/* Exit this routine */
	return;
}


