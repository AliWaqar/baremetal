#include <sys/stat.h>

/*
 * Global Data
 */
enum {
	UART_FR_RXFE = 0x10,
	UART_FR_TXFE = 0x20,
	UART0_ADDR = 0x10009000,
};

#define UART_DR(baseaddr) (*(unsigned int *)(baseaddr))
#define UART_FR(baseaddr) (*(((unsigned int *)(baseaddr))+6))

char *heap_end = 0;

/*
 * Function Definitions
 */
int _close(int file)
{
	return -1;
}

int _isatty(int file)
{
	return 1;
}

int _lseek(int file, int ptr, int dir)
{
	return 0;
}

int _open(const char *name, int flags, int mode)
{
	return -1;
}

int _fstat(int file, struct stat *st)
{
	st->st_mode = S_IFCHR;
	return 0;
}

int _read(int file, char *ptr, int len)
{
	int todo;

	if (len == 0)
	{
		return 0;
	}
	
	while(UART_FR(UART0_ADDR) & UART_FR_RXFE);
	*ptr++ = UART_DR(UART0_ADDR);
	
	for(todo = 1; todo < len; todo++)
	{
		if(UART_FR(UART0_ADDR) & UART_FR_RXFE)
		{
			break;
		}

		*ptr++ = UART_DR(UART0_ADDR);
	}

	return todo;
}

int _write(int file, char *ptr, int len)
{
	int todo;

	for(todo = 0; todo < len; todo++)
	{
		UART_DR(UART0_ADDR) = *ptr++;
	}

	return len;
}

caddr_t _sbrk(int incr)
{
	extern char heap_low;	/* Defined in linker script */
	extern char heap_top;	/* Defined in linker script */
	char *prev_heap_end;

	if(heap_end == 0)
	{
		heap_end = &heap_low;
	}

	prev_heap_end = heap_end;

	if(heap_end + incr > &heap_top)
	{
		/* Heap and stack collision */
		return (caddr_t)0;
	}

	heap_end += incr;
	
	return (caddr_t) prev_heap_end;
}
